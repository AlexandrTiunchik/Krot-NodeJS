module.exports = {
    role: {
        user: 'user',
        admin: 'admin',
        manager: 'manager'
    }
};
