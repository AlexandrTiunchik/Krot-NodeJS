const mongoose = require('mongoose');
const schema = mongoose.Schema;

const basketSchema = new schema({
    user: {
        type: schema.Types.ObjectId,
        ref: 'user'
    },
    dishes: [
        {
            dish: {
                type: schema.Types.ObjectId,
                ref: 'dishes'
            },
            count: {
                type: Number 
            }
        }
    ],
    price: {
        type: String
    }
});

module.exports = Basket = mongoose.model('basket', basketSchema);
