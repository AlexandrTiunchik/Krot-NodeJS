const mongoose = require('mongoose');
const schema = mongoose.Schema;

const userSchema = new schema({
    profile: {
        type: schema.Types.ObjectId,
        ref: 'profile'
    },
    basket: {
        type: schema.Types.ObjectId,
        ref: 'basket'
    },
    loginName: {
        type: String
    },
    password: {
        type: String
    },
    email: {
      type: String,
    },
    avatar: {
        type: String
    },
    date: {
        type: Date,
        default: Date.now
    },
    role: {
        type: String,
        default: 'user'
    }
});

module.exports = User = mongoose.model('user', userSchema);
