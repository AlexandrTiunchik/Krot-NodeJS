const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Dishes = require('../schemas/dishes');

const commentSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'user',
    },
    dish: {
        type: Schema.Types.ObjectId,
        ref: 'dishes',
    },
    date: {
        type: Date,
        default: Date.now
    },
    text: {
        type: String
    },
    rating: {
        type: Number
    }
});

module.exports = Comment = mongoose.model('comment', commentSchema);
