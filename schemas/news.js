const mongoose = require('mongoose');
const schema = mongoose.Schema;

const newsSchema = new schema({
    title: {
        type: String,
        default: ''
    },
    text: {
        type: String,
        default: ''
    },
    link: {
        type: String,
        default: ''
    },
    image: {
        type: String,
        default: ''
    },
    date: {
        type: Date,
        default: Date.now
    },
    video: {
        type: String,
        default: ''
    }
});

module.exports = News = mongoose.model('news', newsSchema);
