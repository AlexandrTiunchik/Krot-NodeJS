const mongoose = require('mongoose');
const schema = mongoose.Schema;

const dishesSchema = new schema({
    title: {
        type: String
    },
    image: {
        type: String
    },
    description: {
        type: String
    },
    date: {
        type: Date,
        default: Date.now
    },
    category: {
        type: String
    },
    video: {
        type: String,
        default: ''
    }
});

module.exports = Dishes = mongoose.model('dishes', dishesSchema);
