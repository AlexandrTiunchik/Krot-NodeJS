const mongoose = require('mongoose');
const schema = mongoose.Schema;

const profileSchema = new schema({
    user: {
        type: schema.Types.ObjectId,
        ref: 'user'
    },
    name: {
        type: String,
        default: ''
    },
    surname: {
        type: String,
        default: ''
    },
    patronymic: {
        type: String,
        default: ''
    },
    telephone: {
        type: String,
        default: ''
    },
    city: {
        type: String,
        default: ''
    },
    address: {
        type: String,
        default: ''
    },
    category: {
        type: String,
        default: ''
    }
});

module.exports = Profile = mongoose.model('profile', profileSchema);
