const mongoose = require('mongoose');
const schema = mongoose.Schema;

const orderSchema = new schema({
    user: {
        type: schema.Types.ObjectId,
        ref: 'user'
    },
    dishes: [
        {
            dish: {
                type: schema.Types.ObjectId,
                ref: 'dishes'
            },
            count: {
                type: Number 
            }
        }
    ],
    status: {
        type: String
    },
    date: {
        type: Date,
        default: Date.now
    },
    price: {
        type: Number
    }
})

module.exports = Order = mongoose.model('order', orderSchema);
