const mongoose = require('mongoose');
const dbUrl = require('../configs/keys/keys').mongoDBURL;

mongoose.connect(dbUrl, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})
    .then(() => console.log('DB connected successfully'))
    .catch(err => console.log(err));

module.exports = mongoose;
