exports.MAIN_PAGE_POPULAR = [
    {
        $lookup: {
              from: "dishes",
              localField: "dish",
              foreignField: "_id",
              as: "dish"
        }
    },
    { $unwind: "$dish" },
    { "$group": { 
        "_id": '$dish._id',
        "dish": {"$first": "$dish"},
        "avgRating": {"$avg": {"$sum": "$rating"}}
    }},
    { "$project":{
        "_id": '$_id',
        "dish": "$dish",
        "roundedAvg" : { "$ceil": '$avgRating' }
    }},
    { "$sort": { "roundedAvg": -1 } },
    { "$limit": 4 }
];
