const errorModel = require('../models/error');

module.exports = (role) => (req, res, next) => {
    if (!role.includes(req.user.role)) {
        const error = new errorModel('roleError', 'You have no permission for this operation');
        return res.status(400).json(error);   
    }

    next();
};
