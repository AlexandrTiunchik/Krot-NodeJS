const express = require('express');
const router = express.Router();
const passport = require('passport');

const role = require('../middleware/role');
const userRole = require('../constants/user').role;
const Profile = require('../schemas/profile');
const errorModel = require('../models/error');

/**
 * Access: User, Admin, Manager
 * Type: Put
 * URL: /api/profile/
 * Update user profile route
 */
router.put('/',
  passport.authenticate('jwt', {session: false}),
  role([userRole.admin, userRole.user, userRole.manager]),
  async(req, res) => {

    try {

        const updateProfile = {
            name: req.body.name,
            surname: req.body.surname,
            patronymic: req.body.patronymic,
            telephone: req.body.telephone,
            city: req.body.city,
            address: req.body.address
        };

        const profile = await Profile.findOneAndUpdate(
            {user: req.user.id},
            {$set: updateProfile}
        );

        if (!profile) {
            const error = new errorModel('dishesError', 'Dish not found');
            return res.status(401).json(error);
        }

        res.status(200).json({status: 'Updated'});

    } catch (err) {
        const error = new errorModel('serverError', 'Error');
        res.status(401).json(error);
    }
});

/**
 * Access: User, Admin, Manager
 * Type: Get
 * URL: /api/profile/
 * Get user profile route
 */
router.get('/',
  passport.authenticate('jwt', {session: false}),
  role([userRole.admin, userRole.user, userRole.manager]),
  async(req, res) => {

    try {
        const profile = await Profile.findOne({user: req.user.id});

        res.status(200).json(profile);

    } catch (err) {
        const error = new errorModel('profileError', 'Error');
        res.status(401).json(error);
    }
});

module.exports = router;
