const express = require('express');
const router = express.Router();
const passport = require('passport');

const Order = require('../schemas/order');
const role = require('../middleware/role');
const userRole = require('../constants/user').role;
const errorModel = require('../models/error');
const Comment = require('../schemas/comment');

/**
 * Access: Admin, User, Manager
 * Type: Post
 * URL: /api/comment/
 * Create comment route
 */
router.post('/:id',
    passport.authenticate('jwt', {session: false}),
    role([userRole.admin, userRole.user, userRole.manager]),
    async (req, res) => {

        try {

            const newComment = new Comment({
                user: req.user.id,
                dish: req.params.id,
                text: req.body.text,
                rating: req.body.rating
            });

            await newComment.save();

            res.status(200).json({});

        } catch (err) {
            const error = new errorModel('serverError', 'Error');
            res.status(401).json(error);
        }

});

/**
 * Access: Admin, User, Manager
 * Type: Delete
 * URL: /api/comment/
 * Delete comment route
 */
router.delete('/:id',
    passport.authenticate('jwt', {session: false}),
    role([userRole.admin, userRole.user, userRole.manager]),
    async (req, res) => {

        try {

            await Comment.findOneAndRemove({
                dish: req.params.id,
                user: req.user.id
            });

            res.status(200).json({});

        } catch (err) {
            const error = new errorModel('serverError', 'Error');
            res.status(401).json(error);
        }

});

/**
 * Access: 
 * Type: Get
 * URL: /api/comment/user
 * Get user comment route
 */
router.get('/user', passport.authenticate('jwt', {session: false}), async (req, res) => {

    try {

        const userComment = await Comment.find({
            dish: req.query.dishId,
            user: req.user.id
        }).populate({
            path: 'user',
            populate: {
                path: 'profile'
            }
        });

        res.status(200).json(userComment);

    } catch (err) {
        const error = new errorModel('serverError', 'Error');
        res.status(401).json(error);
    }
});

/**
 * Access: Publick
 * Type: Get
 * URL: /api/comment/
 * Get dish comments route
 */
router.get('/:id', async (req, res) => {

    try {

        const comments = await Comment.find({dish: req.params.id})
            .populate({
                path: 'user',
                populate: {
                    path: 'profile'
                }
            });

        res.status(200).json(comments);

    } catch (err) {
        const error = new errorModel('serverError', 'Error');
        res.status(401).json(error);
    }

});

module.exports = router;
