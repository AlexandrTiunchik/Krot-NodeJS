const express = require('express');
const router = express.Router();
const passport = require('passport');

const role = require('../middleware/role');
const errorModel = require('../models/error');
const userRole = require('../constants/user').role;
const Basket = require('../schemas/basket');

/**
 * Access: Admin, User, Manager
 * Type: Put
 * URL: /api/basket/
 * Update basket route
 */
router.put('/',
  passport.authenticate('jwt', {session: false}),
  role([userRole.admin, userRole.manager, userRole.user]),
  async (req, res) => {
    
    try {

        const findedBasket = await Basket.findOne({
            user: req.user.id
        });

        const findedDish = findedBasket.dishes.find(x => x.dish == req.body.dish._id);

        if (findedDish) {
            const error = new errorModel('basketError', 'Dish already exist in basket');
            return res.status(401).json(error);
        }

        const updateBasket = {
            dish: req.body.dish._id,
            count: req.body.count
        };

        await Basket.findOneAndUpdate(
            {user: req.user.id},
            {$push: {dishes: updateBasket}}
        );

        const basket = await Basket.findOne({user: req.user.id})
            .populate({
                path: 'dishes.dish',
                model: 'dishes'
            });


        const price = basket.dishes.reduce((a, b) => a + b.dish.price * b.count, 0);

        await Basket.findOneAndUpdate(
            {user: req.user.id},
            {price}
        );

        res.status(200).json(basket);

    } catch (err) {
        const error = new errorModel('serverError', 'Error');
        res.status(401).json(error);
    }
    
});

/**
 * Access: Admin, User, Manager
 * Type: Delete
 * URL: /api/basket/delete
 * Delete item from basket route
 */
router.put('/delete',
  passport.authenticate('jwt', {session: false}),
  role([userRole.admin, userRole.manager, userRole.user]),
  async (req, res) => {
    
    try {

        const updateBasket = {
            dishes: req.body.dishes
        };

        await Basket.findOneAndUpdate(
            {user: req.user.id},
            {$set: updateBasket}
        );

        const basket = await Basket.findOne({user: req.user.id})
            .populate({
                path: 'dishes.dish',
                model: 'dishes'
            });


        const price = basket.dishes.reduce((a, b) => a + b.dish.price * b.count, 0);

        await Basket.findOneAndUpdate(
            {user: req.user.id},
            {price}
        );

        res.status(200).json({});

    } catch (err) {
        const error = new errorModel('serverError', 'Error');
        res.status(401).json(error);
    }
    
});

/**
 * Access: Admin, User, Manager
 * Type: Get
 * URL: /api/basket/
 * Get basket route
 */
router.get('/',
  passport.authenticate('jwt', {session: false}),
  role([userRole.admin, userRole.manager, userRole.user]),
  async(req, res) => {

    try {

        const basket = await Basket.findOne({user: req.user.id})
            .populate({
                path: 'dishes.dish',
                model: 'dishes'
            });

        res.status(200).json(basket);

    } catch (err) {
        const error = new errorModel('serverError', 'Error');
        res.status(401).json(error);
    }

});

module.exports = router;
