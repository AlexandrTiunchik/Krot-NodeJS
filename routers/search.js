const express = require('express');
const router = express.Router();
const errorModel = require('../models/error');

const Dishes = require('../schemas/dishes');

router.get('/',
  async (req, res) => {

    try {
      const { search } = req.query;

      if(search.length === 0) {
        return  res.status(200).json([])
      }

      const dishes = await Dishes.find({
        title: {$regex: search}
      })

      res.status(200).json(dishes)
    } catch (err) {
      const error = new errorModel('serverError', 'Error');
      res.status(401).json(error);
    }
});

module.exports = router;