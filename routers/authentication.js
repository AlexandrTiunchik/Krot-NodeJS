const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const jwtToken = require('jsonwebtoken');
const { check, validationResult } = require('express-validator');

const User = require('../schemas/user');
const Basket = require('../schemas/basket');
const Profile = require('../schemas/profile');
const secretOrPrivateKey = require('../configs/keys/keys').secretOrKey;
const errorModel = require('../models/error');

/**
 * Access: Public
 * Type: Post
 * URL: /api/authentication/login
 * Login route
 */
router.post('/login', [
    check('loginName')
        .isLength({min: 3, max: 12})
        .withMessage('Login must be longer than 3 and shorter than 12 characters'),
    check('password')
        .isLength({min: 6, max: 12})
        .withMessage('Password must be longer than 3 and shorter than 12 characters')
], async(req, res) => {

    try {

        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            return res.status(401).json({errors: errors.array()});
        }

        const user = await User.findOne({loginName: req.body.loginName});

        if (!user) {
            const error = new errorModel('loginNameError', 'User not found');
            return res.status(401).json(error);
        }

        const isPasswordCompare = await bcrypt.compare(req.body.password, user.password);

        if(!isPasswordCompare) {
            const error = new errorModel('passwordError', 'Password incorrect');
            return res.status(401).json(error);
        }

        const tokenPayload = {
            id: user.id,
            loginName: user.loginName,
            role: user.role
        };

        const token = jwtToken.sign(
            tokenPayload,
            secretOrPrivateKey,
            {expiresIn: '60d'},
        );

        res.status(200).json({
            token
        });

    } catch (err) {
        const error = new errorModel('serverError', 'Error');
        res.status(401).json(error);
    }
});

/**
 * Access: Public
 * Type: Post
 * URL: /api/authentication/registration
 * Registration route
 */
router.post('/registration', [
    check('loginName')
        .isLength({min: 3, max: 12})
        .withMessage('Login must be longer than 3 and shorter than 12 characters'),
    check('email').isEmail()
        .withMessage('Invalid email'),
    check('password')
        .isLength({min: 6, max: 12})
        .withMessage('Password must be longer than 3 and shorter than 12 characters')
], async(req, res) => {
    try {

        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            return res.status(401).json({errors: errors.array()});
        }

        const user = await User.findOne({loginName: req.body.loginName});

        if (user) {
            const error = new errorModel('loginNameError', 'User already exist');
            return res.status(401).json(error);
        }

        const email = await User.findOne({email: req.body.email});

        if (email) {
            const error = new errorModel('emailError', 'Email already exist');
            return res.status(401).json(error);
        }

        const newUser = new User({
            loginName: req.body.loginName,
            password: req.body.password,
            email: req.body.email
        });

        const salt = await bcrypt.genSalt(10);

        newUser.password = await bcrypt.hash(newUser.password, salt);

        const createdUser = await newUser.save();

        const createProfile = new Profile({
            user: createdUser.id
        });

        const createBasket = new Basket({
            user: createdUser.id
        });

        const createdProfile = await createProfile.save();

        const createdBasket = await createBasket.save();

        await User.findByIdAndUpdate(createdUser.id, {
            profile: createdProfile.id,
            basket: createdBasket.id
        });

        res.status(200).json({message: 'Registration successfully completed'});

    } catch (err) {
        const error = new errorModel('serverError', 'Error');
        res.status(401).json(error);
    }
});

module.exports = router;
