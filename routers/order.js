const express = require('express');
const router = express.Router();
const passport = require('passport');

const Order = require('../schemas/order');
const role = require('../middleware/role');
const userRole = require('../constants/user').role;
const errorModel = require('../models/error');
const Profile = require('../schemas/profile');
const transporter = require('../mail/nodemailer');
const Basket = require('../schemas/basket');
const User = require('../schemas/user');

/**
 * Access: Admin, User, Manager
 * Type: Post
 * URL: /api/order/
 * Create order route
 */
router.post('/',
  passport.authenticate('jwt', {session: false}),
  role([userRole.admin, userRole.user]),
  async (req, res) => {

    try {

        const newOrder = new Order({
            user: req.user.id,
            status: 'pending',
            price: null
        });

        if (req.body.dishes) {
            newOrder.dishes = req.body.dishes
        } else {
            const basket = await Basket.findOne({user: req.user.id});
            newOrder.dishes = basket.dishes;
        }

        const createdOrder = await newOrder.save();

        const order = await Order.findById(createdOrder.id)
            .populate({
                path: 'dishes.dish',
                model: 'dishes'
            });

        const price = order.dishes.reduce((a, b) => a + b.dish.price * b.count, 0);

        const updateOrder = await Order
            .findByIdAndUpdate(order.id, {price}, {new: true})
            .populate({
                path: 'dishes.dish',
                model: 'dishes'
            });

        const profile = await Profile.findOne({user: req.user.id});

        const finalOrder = updateOrder.dishes.map(item => {
            item.dish.total = item.dish.price * item.count;
            return item;
        });

        const mail = {
            from: '"DELIVERY CLUB"',
            to: `${req.user.email}`,
            subject: "Order information",
            template: 'mail',
            context: {
                user: req.user,
                finalOrder,
                totalPrice: updateOrder.price,
                profile
            }
        };

        await transporter.sendMail(mail);

        await Basket.findOneAndUpdate({
            user: req.user.id,
            dishes: [],
            price: 0
        });

        res.status(200).json(order);

    } catch (err) {
        const error = new errorModel('serverError', 'Error');
        res.status(401).json(error);
    }
});

/**
 * Access: Admin, Manager
 * Type: Put
 * URL: /api/order/:id
 * Update order route
 */
router.put('/:id',
  passport.authenticate('jwt', {session: false}),
  role([userRole.admin]),
  async (req, res) => {

    try {
        const updatedOrder = {
            dishes: req.body.dishes,
            status: req.body.status
        };

        const order = await Order.findByIdAndUpdate(
            req.params.id,
            {$set: updatedOrder},
            {new: true}
            ) .populate({
                path: 'dishes.dish',
                model: 'dishes'
            });

        if (!order) {
            const error = new errorModel('orderError', 'Order not found');
            return res.status(401).json(error);
        }

        const user = await User.findOne({_id: order.user});

        const profile = await Profile.findOne({user: order.user})

        const mail = {
            from: '"DELIVERY CLUB"',
            to: `${req.user.email}`,
            subject: "Order information",
            template: 'mail',
            context: {
                user,
                order,
                profile
            }
        };

        await transporter.sendMail(mail);

        res.status(200).json({status: 'Updated'});

    } catch (err) {
        const error = new errorModel('serverError', 'Error');
        res.status(401).json(error);
    }

});

/**
 * Access: Admin, Manager
 * Type: Delete
 * URL: /api/order/:id
 * Delete order route
 */
router.delete('/:id',
  passport.authenticate('jwt', {session: false}),
  role([userRole.admin]),
  async (req, res) => {

    try {
        const order = await Order.findByIdAndRemove(req.params.id);

        if (!order) {
            const error = new errorModel('orderIrror', 'Dish not found');
            return res.status(401).json(error);
        }

        res.status(200).json({status: 'Delited'});
        
    } catch (err) {
        const error = new errorModel('serverError', 'Error');
        res.status(401).json(error);
    }

})

/**
 * Access: Admin, Manager
 * Type: Get
 * URL: /api/order/all
 * Get order route
 */
router.get('/all',
  passport.authenticate('jwt', {session: false}),
  role([userRole.admin, userRole.manager]),
  async (req, res) => {

    try {
        const orders = await Order.find()
            .populate({
                path: 'user',
                populate: {
                    path: 'profile',
                    model: 'profile'
                }
            })
            .populate({
                path: 'dishes.dish',
                model: 'dishes'
            })

        res.status(200).json(orders);

    } catch (err) {
        const error = new errorModel('serverError', 'Error');
        res.status(401).json(error);
    }

});

/**
 * Access: Admin, Manager, User
 * Type: Get
 * URL: /api/order/user/:id
 * Get user order route
 */
router.get('/user/:id',
  passport.authenticate('jwt', {session: false}),
  role([userRole.admin, userRole.manager, userRole.user]),
  async(req, res) => {

    try {
        const orders = await Order.find({user: req.user.id})
            .populate({
                path: 'dishes.dish',
                model: 'dishes'
            });

        res.status(200).json(orders);

    } catch (err) {
        const error = new errorModel('serverError', 'Error');
        res.status(401).json(error);
    }

});


module.exports = router;
