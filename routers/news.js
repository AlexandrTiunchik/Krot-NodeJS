const express = require('express');
const router = express.Router();
const passport = require('passport');

const role = require('../middleware/role');
const userRole = require('../constants/user').role;
const errorModel = require('../models/error');
const News = require('../schemas/news');

/**
 * Access: Admin
 * Type: Post
 * URL: /api/news/
 * Create news route
 */
router.post('/',
  passport.authenticate('jwt', {session: false}),
  role([userRole.admin]),
  async (req, res) => {
    
    try {

        const newNews = new News({
            title: req.body.title,
            text: req.body.text,
            link: req.body.link
        });

        await newNews.save();

        res.status(200).json({});

    } catch (err) {
        const error = new errorModel('serverError', 'Error');
        res.status(401).json(error);
    }
});

/**
 * Access: All
 * Type: Get
 * URL: /api/news/
 * Get news route
 */
router.get('/', async (req, res) => {

    try {
        const news = await News.find()
            .limit(+req.query.count);

        res.status(200).json(news);

    } catch (err) {
        const error = new errorModel('serverError', 'Error');
        res.status(401).json(error);
    }
});

/**
 * Access: Admin
 * Type: Delete
 * URL: /api/news/:id
 * Delete news route
 */
router.delete('/:id',
  passport.authenticate('jwt', {session: false}),
  role([userRole.admin]),
  async(req, res) => {

    try {

        const news = await News.findByIdAndRemove(req.params.id);

        if (!news) {
            const error = new errorModel('newsIrror', 'News not found');
            return res.status(401).json(error);
        }

        res.status(200).json({status: 'Delited'});
        
    } catch (err) {
        const error = new errorModel('serverError', 'Error');
        res.status(401).json(error);
    }

});

/**
 * Access: Admin
 * Type: Put
 * URL: /api/news/
 * Update news route
 */
router.put('/:id',
  passport.authenticate('jwt', {session: false}),
  role([userRole.admin]),
  async(req, res) => {

    try {

        const updateNews = {
            title: req.body.title,
            text: req.body.text,
            link: req.body.link,
            video: req.body.video,
            image: req.body.image
        };

        const news = await News.findByIdAndUpdate(
            req.params.id,
            {$set: updateNews}
        );

        if (!news) {
            const error = new errorModel('newsError', 'News not found');
            return res.status(401).json(error);
        }

        res.status(200).json({status: 'Updated'});

    } catch (err) {
        const error = new errorModel('serverError', 'Error');
        res.status(401).json(error);
    }
});

module.exports = router;
