const express = require('express');
const router = express.Router();
const errorModel = require('../models/error');
const Comment = require('../schemas/comment');

const Dishes = require('../schemas/dishes');

/**
 * Access: Public
 * Type: Get
 * URL: /api/menu/
 * Get menu route
 */
router.get('/', async(req, res) => {

    try {

        const options = {
            sort: {
                price: null,
                date: null,
                rating: null
            },
            skip: +req.query.skip,
            limit: +req.query.limit
        };

        let sortOption;

        if (req.query.sort) {
            sortOption = req.query.sort.split('|');
            options.sort[sortOption[0]] = +sortOption[1]
        }

        const searchOptions = {};

        if (options.sort.rating) {
            const pipeline = [
                {
                    $lookup: {
                        from: "dishes",
                        localField: "dish",
                        foreignField: "_id",
                        as: "dish"
                    }
                },
                { $unwind: "$dish" },
                { "$group": { 
                    "_id": '$dish._id',
                    "dish": {"$first": "$dish"},
                    "avgRating": {"$avg": {"$sum": "$rating"}}
                }},
                { "$project":{
                    "_id": '$_id',
                    "dish": "$dish",
                    "roundedAvg" : { "$ceil": '$avgRating' }
                }},
                { "$sort": { "roundedAvg": options.sort.rating } },
            ];

            if (req.query.category) {
                pipeline.splice(2, 0, {"$match": {"dish.category": req.query.category}})
            }

            const sortedDishes = await Comment.aggregate(pipeline);

            const dishes = sortedDishes.map(item => {
                item.dish.rating = item.roundedAvg;
                return item.dish;
            });

            return res.status(200).json(dishes);
        }

        if (req.query.category) {
            searchOptions.category = req.query.category
        }

        const dishes = await Dishes.find(
            searchOptions,
            [],
            options
        );

        res.status(200).json(dishes);

    } catch (err) {
        const error = new errorModel('serverError', 'Error');
        res.status(401).json(error);
    }

});

module.exports = router;
