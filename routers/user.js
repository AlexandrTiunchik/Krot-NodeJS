const express = require('express');
const router = express.Router();
const passport = require('passport');

const User = require('../schemas/user');
const errorModel = require('../models/error');

/**
 * Access: User, Admin, Manager
 * Type: Put
 * URL: /api/profile/
 * Update user profile route
 */
router.get('/',
  passport.authenticate('jwt', {session: false}),
  async (req, res) => {

    try {

        const foundUser = await User.findById(req.user.id);

        if (!foundUser) {
            const error = new errorModel('userError', 'User not found');
            return res.status(401).json(error);
        }

        const user = {
            id: foundUser.id,
            loginName: foundUser.loginName,
            role: foundUser.role
        };

        res.status(200).json(user);

    } catch (err) {
        const error = new errorModel('serverError', 'Error');
        res.status(401).json(error);
    }

});

module.exports = router;
