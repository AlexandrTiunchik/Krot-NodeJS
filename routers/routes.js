const authenticationRouter = require('./authentication');
const dishesRouter = require('./dishes');
const profileRouter = require('./profile');
const orderRouter = require('./order');
const basketRouter = require('./basket');
const userRouter = require('./user');
const newsRouter = require('./news');
const commentRouter = require('./comment');
const menuRouter = require('./menu');
const searchRouter = require('./search');

function routes(app) {
    app.use('/api/authentication', authenticationRouter);
    app.use('/api/dishes', dishesRouter);
    app.use('/api/profile', profileRouter);
    app.use('/api/order', orderRouter);
    app.use('/api/basket', basketRouter);
    app.use('/api/user', userRouter);
    app.use('/api/news', newsRouter);
    app.use('/api/comment', commentRouter);
    app.use('/api/menu', menuRouter);
    app.use('/api/search', searchRouter)
}

module.exports = routes;