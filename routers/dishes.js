const express = require('express');
const router = express.Router();
const passport = require('passport');

const role = require('../middleware/role');
const Dishes = require('../schemas/dishes');
const userRole = require('../constants/user').role;
const errorModel = require('../models/error');
const Comment = require('../schemas/comment');
const {MAIN_PAGE_POPULAR} = require('../db/pipelines');

/**
 * Access: Public
 * Type: Get
 * URL: /api/dishes/
 * Get dishes route
 */
router.get('/', async (req, res) => {
   try {

    const dishes = await Comment.aggregate(MAIN_PAGE_POPULAR);

    res.status(200).json(dishes);

   } catch (err) {
    const error = new errorModel('serverError', 'Error');
    res.status(401).json(error);
   }
});

/**
 * Access: Admin
 * Type: Post
 * URL: /api/dishes/
 * Create dish route
 */
router.post('/',
  passport.authenticate('jwt', {session: false}),
  role([userRole.admin]),
  async(req, res) => {
    
    try {
        
        const ingredients = req.body.ingredients.split(',');

        const newDishes = new Dishes({
            title: req.body.title,
            image: req.body.image,
            description: req.body.description,
            video: req.body.video,
            price: req.body.price,
            ingredients,
            category: req.body.category
        });

        const dish = await newDishes.save();

        res.status(200).json(dish);

    } catch (err) {
        const error = new errorModel('serverError', 'Error');
        res.status(401).json(error);
    }

});

/**
 * Access: Admin
 * Type: Delete
 * URL: /api/dishes/:id
 * Delete dish route
 */
router.delete('/:id',
  passport.authenticate('jwt', {session: false}),
  role([userRole.admin]),
  async(req, res) => {

    try {

        const dish = await Dishes.findByIdAndRemove(req.params.id);

        if (!dish) {
            const error = new errorModel('dishesIrror', 'Dish not found');
            return res.status(401).json(error);
        }

        res.status(200).json({status: 'Delited'});
        
    } catch (err) {
        const error = new errorModel('serverError', 'Error');
        res.status(401).json(error);
    }

});

/**
 * Access: Admin
 * Type: Put
 * URL: /api/dishes/:id
 * Update dish route
 */
router.put('/:id',
  passport.authenticate('jwt', {session: false}),
  role([userRole.admin]),
  async(req, res) => {

    try {

        let ingredients = [];

        if (Array.isArray(req.body.ingredients)) {
            ingredients = req.body.ingredients;
        } else {
            ingredients = req.body.ingredients.split(',');
        }

        const updateDishes = {
            title: req.body.title,
            image: req.body.image,
            description: req.body.description,
            video: req.body.video,
            ingredients,
            category: req.body.category
        };

        const dish = await Dishes.findByIdAndUpdate(
            req.params.id,
            {$set: updateDishes}
            );

        if (!dish) {
            const error = new errorModel('dishesIrror', 'Dish not found');
            return res.status(401).json(error);
        }

        res.status(200).json({status: 'Updated'});

    } catch (err) {
        const error = new errorModel('serverError', 'Error');
        res.status(401).json(error);
    }

});

/**
 * Access: Public
 * Type: Get
 * URL: /api/dishes/all
 * Get dishes route
 */
router.get('/all', async(req, res) => {

    try {

        const dishes = await Dishes.find();

        res.status(200).json(dishes);

    } catch (err) {
        const error = new errorModel('serverError', 'Error');
        res.status(401).json(error);
    }

});

/**
 * Access: Public
 * Type: Get
 * URL: /api/dishes/:id
 * Get dish route
 */
router.get('/:id', async(req, res) => {

    try {

        const dish = await Dishes.findById(req.params.id);

        if (!dish) {
            const error = new errorModel('dishesIrror', 'Dish not found');
            return res.status(401).json(error);
        }

        res.status(200).json(dish);

    } catch (err) {
        const error = new errorModel('serverError', 'Error');
        res.status(401).json(error);
    }

});

module.exports = router;
