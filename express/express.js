const express = require('express');
const app = express();

const cors = require('cors');
const bodyParser = require('body-parser');
const passport = require('passport');
require('../middleware/passport')(passport);
const routes = require('../routers/routes');

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use(passport.initialize());
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

routes(app);

module.exports = app;