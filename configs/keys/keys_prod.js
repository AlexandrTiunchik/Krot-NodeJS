module.exports = {
    mongoDBURL: process.env.MONGO_URI,
    secretOrKey: process.env.SECRET_OR_KEY,
    mailAddress: process.env.MAIL_ADDRESS,
    mailPassword: process.env.MAIL_PASSWORD
};
