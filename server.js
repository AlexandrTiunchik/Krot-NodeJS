const app = require('./express/express');
require('./db/mongoose');
const path = require('path');

const PORT = process.env.PORT || 3100;

app.set('views', path.join(__dirname, 'views'))

app.listen(PORT, () => console.log(`server start on port ${PORT}`));
