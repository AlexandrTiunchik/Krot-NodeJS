const nodemailer = require('nodemailer');
const hbs = require('nodemailer-express-handlebars');
const path = require('path');
const keys = require('../configs/keys/keys');

const transporter = nodemailer.createTransport({
    service: 'gmail',
    secure: false,
    port: 587,
    auth: {
        user: keys.emailAddress,
        pass: keys.emailPassword
    }
});

transporter.use('compile', hbs({
    viewEngine: {
        extName: '.handlebars',
        defaultLayout: 'mail',
        partialsDir: path.join(__dirname, '../views'),
        layoutsDir: path.join(__dirname, '../views')
    },
    viewPath: path.join(__dirname, '../views'),
}));

module.exports = transporter;